const kb = require("./keyboardButtons");

module.exports = {
  home: [[kb.home.films, kb.home.cinemas], [kb.home.favourite]],
  films: [[kb.film.random], [kb.film.actions, kb.film.comedy], [kb.back]],
  cinemas: [[{ text: "Send location", request_location: true }], [kb.back]]
};
