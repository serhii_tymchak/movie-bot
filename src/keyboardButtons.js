module.exports = {
  home: {
    films: "In cinema now",
    favourite: "Favorite",
    cinemas: "Cinemas"
  },
  film: {
    random: "Random genre",
    actions: "Actions",
    comedy: "Comedy"
  },
  back: "Go back"
};
