const TelegramBot = require("node-telegram-bot-api");
const config = require("./config");
const helper = require("./helper");
const keyboard = require("./keyboard");
const kb = require("./keyboardButtons");
const mongoose = require("mongoose");
const geolib = require("geolib");
const _ = require("lodash");

helper.logStart();

mongoose.Promise = global.Promise;
mongoose
  .connect(config.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("MongoDB connected"))
  .catch(err => console.log("Conection failed!"));

require("./models/film.model");
require("./models/cinema.model");
require("./models/user.model");

const Film = mongoose.model("films");
const Cinema = mongoose.model("cinemas");
const User = mongoose.model("users");

const ACTION_TYPE = {
  TOGGLE_FAV_FILM: "tff",
  SHOW_CINEMAS: "sc",
  SHOW_CINEMAS_MAP: "scm",
  SHOW_FILMS: "sf"
};

const bot = new TelegramBot(config.TOKEN, {
  polling: true
});

bot.on("message", msg => {
  const chatId = helper.getChatId(msg);

  switch (msg.text) {
    case kb.home.favourite:
      showFavoriteFilms(chatId, msg.from.id);
      break;
    case kb.home.films:
      bot.sendMessage(chatId, "Choose the genre:", {
        reply_markup: { keyboard: keyboard.films }
      });
      break;
    case kb.film.comedy:
      sendFilmsByQuery(chatId, { type: "comedy" });
      break;
    case kb.film.actions:
      sendFilmsByQuery(chatId, { type: "action" });
      break;
    case kb.film.random:
      sendFilmsByQuery(chatId, {});
      break;
    case kb.home.cinemas:
      bot.sendMessage(chatId, "Send location", {
        reply_markup: { keyboard: keyboard.cinemas }
      });
      break;
    case kb.back:
      bot.sendMessage(chatId, "What do you wish to watch?", {
        reply_markup: { keyboard: keyboard.home }
      });
      break;
  }
  if (msg.location) {
    getCinemasInCoord(chatId, msg.location);
  }
});

bot.onText(/\/start/, msg => {
  const chatId = helper.getChatId(msg);
  const text = `Greetings, ${msg.from.first_name}\nChoose the command to start work:`;

  bot.sendMessage(chatId, text, {
    reply_markup: {
      keyboard: keyboard.home
    }
  });
});

bot.onText(/\/f(.+)/, (msg, [sourse, match]) => {
  const filmUuid = helper.getItemUuid(sourse);
  const chatId = helper.getChatId(msg);

  Promise.all([
    Film.findOne({ uuid: filmUuid }),
    User.findOne({ telegramId: msg.from.id })
  ]).then(([film, user]) => {
    let isFav = false;

    if (user) {
      isFav = user.films.indexOf(film.uuid) !== -1;
    }

    const favText = isFav ? "Delete from favorite" : "Add to favorite";

    const caption = `Film name: ${film.name}\nYear: ${film.year}\nRate: ${film.rate}\nDuration: ${film.length}\nCountry: ${film.country}`;
    bot.sendPhoto(chatId, film.picture, {
      caption: caption,
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: favText,
              callback_data: JSON.stringify({
                type: ACTION_TYPE.TOGGLE_FAV_FILM,
                filmUuid: film.uuid,
                isFav: isFav
              })
            },
            {
              text: "Show cinemas",
              callback_data: JSON.stringify({
                type: ACTION_TYPE.SHOW_CINEMAS,
                cinemaUuids: film.cinemas
              })
            }
          ],
          [
            {
              text: `Cinema search ${film.name}`,
              url: film.link
            }
          ]
        ]
      }
    });
  });
});

bot.onText(/\/c(.+)/, (msg, [sourse, match]) => {
  const cinemaUuid = helper.getItemUuid(sourse);
  const chatId = helper.getChatId(msg);

  Cinema.findOne({ uuid: cinemaUuid }).then(cinema => {
    bot.sendMessage(chatId, `Cinema ${cinema.name}`, {
      reply_markup: {
        inline_keyboard: [
          [
            {
              text: cinema.name,
              url: cinema.url
            }
          ],
          [
            {
              text: "Show on map",
              callback_data: JSON.stringify({
                type: ACTION_TYPE.SHOW_CINEMAS_MAP,
                lat: cinema.location.latitude,
                lon: cinema.location.longitude
              })
            }
          ],
          [
            {
              text: "Show films",
              callback_data: JSON.stringify({
                type: ACTION_TYPE.SHOW_FILMS,
                filmUuids: cinema.films
              })
            }
          ]
        ]
      }
    });
  });
});

bot.on("callback_query", query => {
  const userId = query.from.id;

  let data;

  try {
    data = JSON.parse(query.data);
  } catch (e) {
    throw new Error("Data is not and object");
  }

  const { type } = data;

  switch (type) {
    case ACTION_TYPE.SHOW_CINEMAS_MAP:
      console.log("SHOW SINEMAS MAP");
      break;
    case ACTION_TYPE.SHOW_CINEMAS:
      console.log("SHOW_CINEMAS");
      break;
    case ACTION_TYPE.TOGGLE_FAV_FILM:
      toggleFavoriteFilm(userId, query.id, data);
      break;
    case ACTION_TYPE.SHOW_FILMS:
      console.log("TOGGLE_FAV_FILM");
      break;
    default:
      console.log("<<<<<<<<<<<");
  }
});

function sendFilmsByQuery(chatId, query) {
  Film.find(query).then(films => {
    const html = films
      .map((film, index) => {
        return `<b>${index + 1}</b> ${film.name} - /f${film.uuid}`;
      })
      .join("\n");

    sendHTML(chatId, html, "films");
  });
}

function sendHTML(chatId, html, kbName = null) {
  const options = {
    parse_mode: "HTML"
  };

  if (kbName) {
    options["reply_markup"] = {
      keyboard: keyboard[kbName]
    };
  }

  bot.sendMessage(chatId, html, options);
}

function getCinemasInCoord(chatId, location) {
  Cinema.find({}).then(cinemas => {
    cinemas.forEach(c => {
      c.distance = geolib.getDistance(location, c.location) / 1000;
    });

    cinemas = _.sortBy(cinemas, "distance");

    const html = cinemas
      .map((c, i) => {
        return `<b>${i + 1}</b> ${c.name}. <em>Distance</em> - <strong>${
          c.distance
        } km.</strong> /c${c.uuid}`;
      })
      .join("\n");
    sendHTML(chatId, html, "home");
  });
}

function toggleFavoriteFilm(userId, queryId, { filmUuid, isFav }) {
  let userPromise;

  User.findOne({ telegramId: userId })
    .then(user => {
      if (user) {
        if (isFav) {
          user.films = user.films.filter(fUuid => fUuid !== filmUuid);
        } else {
          user.films.push(filmUuid);
        }
        userPromise = user;
      } else {
        userPromise = new User({
          telegramId: userId,
          films: [filmUuid]
        });
      }

      const answerText = isFav ? "Film delited" : "Film added";

      userPromise
        .save()
        .then(_ => {
          bot.answerCallbackQuery({
            callback_query_id: queryId,
            text: answerText
          });
        })
        .catch(err => console.log(err));
    })
    .catch(err => console.log(err));
}

function showFavoriteFilms(chatId, telegramId) {
  User.findOne({ telegramId })
    .then(user => {
      if (user) {
        Film.find({ uuid: { $in: user.films } })
          .then(films => {
            let html;
            if (films.length) {
              html = films
                .map((f, i) => {
                  return `<b>${i + 1}</b> ${f.name} - <b>${f.rate}</b> (/f${
                    f.uuid
                  })`;
                })
                .join("\n");
            } else {
              html = "You added nothing yet.";
            }
            sendHTML(chatId, html, "home");
          })
          .catch(e => console.log(e));
      } else {
        sendHTML(chatId, "You added nothing yet.", "home");
      }
    })
    .catch(e => console.log(e));
}
